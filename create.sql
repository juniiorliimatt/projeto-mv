create sequence hibernate_sequence start 1 increment 1;
create table establishments (id int8 not null, adress bytea, name varchar(255), phone_number varchar(255), primary key (id));
create table professionals (id int8 not null, adress bytea, function varchar(255), name varchar(255), phone_number varchar(255), residential_phone_number varchar(255), primary key (id));
