package com.mv.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mv.entities.Establishment;
import com.mv.services.EstablishmentService;

@RestController
@RequestMapping("/api/establishment")
public class EstablishmentController {

	@Autowired
	private EstablishmentService service;

	@GetMapping("/")
	public ResponseEntity<List<Establishment>> findAll() {
		return ResponseEntity.status(HttpStatus.OK).body(service.findAll());
	}

	@GetMapping("/{id}")
	public ResponseEntity<Establishment> findById(@PathVariable Long id) {
		if (establishmentFromDB(id).isEmpty()) {
			return notFound();
		}
		return ResponseEntity.status(HttpStatus.OK).body(establishmentFromDB(id).get());
	}

	@PostMapping
	public ResponseEntity<Establishment> save(@RequestBody Establishment establishment) {
		service.save(establishment);
		return ResponseEntity.status(HttpStatus.CREATED).build();
	}

	@PutMapping("/{id}")
	public ResponseEntity<Establishment> update(@RequestBody Establishment establishment,
			@PathVariable Long id) {
		if (establishmentFromDB(id).isEmpty()) {
			return notFound();
		}
		service.update(establishment, id);
		return ResponseEntity.status(HttpStatus.OK).build();
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Establishment> delete(@PathVariable Long id) {
		if (establishmentFromDB(id).isEmpty()) {
			return notFound();
		}
		service.delete(id);
		return ResponseEntity.status(HttpStatus.OK).build();
	}

	private ResponseEntity<Establishment> notFound() {
		return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
	}

	private Optional<Establishment> establishmentFromDB(Long id) {
		Optional<Establishment> establishmentFromDB = service.findById(id);
		return establishmentFromDB;
	}
}
