package com.mv.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mv.entities.Professional;
import com.mv.repositories.ProfessionalRepository;

@Service
public class ProfessionalService {

	@Autowired
	private ProfessionalRepository repository;

	@Transactional(readOnly = true)
	public List<Professional> findAll() {
		return repository.findAll();
	}

	@Transactional(readOnly = true)
	public Optional<Professional> findById(Long id) {
		return repository.findById(id);
	}

	@Transactional
	public Professional save(Professional professional) {
		return repository.save(professional);
	}

	@Transactional
	public Professional update(Professional professional, Long id) {
		Optional<Professional> professionalFromDB = findById(id);

		if (professionalFromDB.isPresent()) {
			Professional update = professionalFromDB.get();
			update.setName(professional.getName());
			update.setPhoneNumber(professional.getPhoneNumber());
			update.setResidentialPhoneNumber(professional.getResidentialPhoneNumber());
			update.setAddress(professional.getAddress());
			update.setFunction(professional.getFunction());
			return save(update);
		}
		return professionalFromDB.get();
	}

	@Transactional
	public void delete(Long id) {
		Optional<Professional> professionalFromDB = findById(id);
		if (professionalFromDB.isPresent()) {
			repository.deleteById(id);
		}
	}
}